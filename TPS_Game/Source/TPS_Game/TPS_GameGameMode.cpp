// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_GameGameMode.h"
#include "TPS_GamePlayerController.h"
#include "TPS_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_GameGameMode::ATPS_GameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_GamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprint/Character/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
} 