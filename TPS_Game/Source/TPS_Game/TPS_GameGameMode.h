// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_GameGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_GameGameMode();
};



